using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject player;

    public Vector3 initDiff;
    public Vector3 diff;
    public float camSpeed = 1f;
    // Start is called before the first frame update
    void Start()
    {
        initDiff = player.transform.position - transform.position;
        diff = initDiff;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, player.transform.position - diff, camSpeed * Time.deltaTime);
    }

    public void HeightChange(int value)
    {
        diff -= new Vector3(0, 1, -1) * value;
    }

    public void ResetDiff()
    {
        diff = initDiff;
    }
}
