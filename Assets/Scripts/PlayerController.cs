using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Transform leftToeBase;
    public Transform rightToeBase;
    public GameObject heelPrefab;
    private GameObject lastLeftHeel;
    private GameObject lastRightHeel;
    public GameObject playerModel;
    public List<GameObject> heelList;
    private Rigidbody rb;
    public bool movementEnabled = false;
    public Camera mainCam;
    public int heelCount = 0;
    public float speed = 10f;
    
    public Animator animator;
    public GameManager gameManager;
    private Rigidbody[] ragdollBodies;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        mainCam = Camera.main;
        rb = GetComponent<Rigidbody>();
        ragdollBodies = playerModel.GetComponentsInChildren<Rigidbody>();
        foreach (var rb in ragdollBodies)
        {
            
            rb.isKinematic = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (movementEnabled)
        {
            transform.position = transform.position + Vector3.forward * (speed * Time.deltaTime);
        }
        
        
    }

    private void Ragdoll()
    {
        
        foreach (var rb in ragdollBodies)
        {
            rb.isKinematic = false;
        }

        playerModel.GetComponent<Animator>().enabled = false;
        
        
    }

    public void Heels()
    {
        
            lastLeftHeel = Instantiate(heelPrefab, leftToeBase);
            lastLeftHeel.name = "Left Heel " + heelCount;
            lastLeftHeel.transform.localPosition = Vector3.zero;
            lastLeftHeel.transform.localPosition += Vector3.back*heelCount;
            lastRightHeel = Instantiate(heelPrefab, rightToeBase);
            lastRightHeel.name = "Right Heel " + heelCount;
            lastRightHeel.transform.localPosition = Vector3.zero;
            lastRightHeel.transform.localPosition += Vector3.back*heelCount;
           
            heelList.Add(lastLeftHeel);
            heelList.Add(lastRightHeel);
            heelCount += 1;
            
        
        
        
        
            PlayerHeight(1);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Obstacle1"))
        {
            Obstacle(1);
        }
        if (other.gameObject.CompareTag("Obstacle2"))
        {
            Obstacle(2);
        }
        if (other.gameObject.CompareTag("Obstacle3"))
        {
            Obstacle(3);
        }
        if (other.gameObject.CompareTag("Obstacle4"))
        {
            Obstacle(4);
        }
        if (other.gameObject.CompareTag("Obstacle5"))
        {
            Obstacle(5);
        }

        if (other.gameObject.CompareTag("Heel"))
        {
            Heels();
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("Final"))
        {
            if (heelCount==0)
            {
                Finish();
                
            }
            else
            {
                Obstacle(1);
            }
            
        }

        if (other.gameObject.CompareTag("Bar"))
        {
            animator.SetBool("Bar", true);
            speed = 5f;
        }
        if (other.gameObject.CompareTag("Fall"))
        {
            Fail();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Obstacle1"))
        {
            PlayerHeight(-1);
        }
        if (other.gameObject.CompareTag("Obstacle2"))
        {
            PlayerHeight(-2);
        }
        if (other.gameObject.CompareTag("Obstacle3"))
        {
            PlayerHeight(-3);
        }
        if (other.gameObject.CompareTag("Obstacle4"))
        {
            PlayerHeight(-4);
        }
        if (other.gameObject.CompareTag("Obstacle5"))
        {
            PlayerHeight(-5);
        }
        if (other.gameObject.CompareTag("Bar"))
        {
            speed = 10f;
            animator.SetBool("Bar", false);
        }
    }

    private void Obstacle(int height)
    {
        if (height<=heelCount)
        {
            
            for (int i = height; i > 0; i--)
            {
                var heel = heelList[heelCount*2-height*2];
                var heel2 = heelList[heelCount*2-height*2+1];
                
                heel.GetComponentInChildren<Rigidbody>().isKinematic = false;
                heel2.GetComponentInChildren<Rigidbody>().isKinematic = false;
                heel.transform.parent = null;
                heel2.transform.parent = null;
                heelList.Remove(heel);
                heelList.Remove(heel2);
            } 
        }
        else
        {
            Fail();
        }
        
        
        heelCount -= height;
    }

    private void PlayerHeight(int value)
    {
        playerModel.transform.DOLocalMove(playerModel.transform.localPosition + (Vector3.up * value), 5f*Mathf.Abs(value)).SetEase(Ease.Linear).SetSpeedBased();;
        mainCam.GetComponent<CameraFollow>().HeightChange(value);
    }

    private void Fail()
    {
        gameManager.FailGame();
        Ragdoll();
        movementEnabled = false;
        GetComponent<InputManager>().inputEnabled = false;
    }

    private void Finish()
    {
        gameManager.FailGame();
        transform.DORotate(new Vector3(0, 180, 0), 1f);
        movementEnabled = false;
        GetComponent<InputManager>().inputEnabled = false;
        animator.SetBool("Victory", true);
    }
}
