using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject player;
    public GameObject playerPrefab;
    public GameObject camera;
    public GameObject restartUI;
    
    public GameObject[] pickups;

    private Vector3 playerInitPos;

    private Quaternion playerInitRot;
    // Start is called before the first frame update
    void Start()
    {
        playerInitPos = player.transform.position;
        playerInitRot = player.transform.rotation;
        pickups = GameObject.FindGameObjectsWithTag("Heel");
    }

    public void FailGame()
    {
        restartUI.SetActive(true);
    }
    public void RestartGame()
    {
        
        foreach (var var in GameObject.FindGameObjectsWithTag("HeelShoe"))
        {
            Destroy(var);
        }
        
        restartUI.SetActive(false);
        Destroy(player);
        GameObject newPlayer = Instantiate(playerPrefab);
        newPlayer.transform.position = playerInitPos;
        newPlayer.transform.rotation = playerInitRot;
        player = newPlayer;
        
        var cameraFollow = camera.GetComponent<CameraFollow>();
        cameraFollow.player = newPlayer;
        cameraFollow.ResetDiff();
        foreach (var var in pickups)
        {
            var.SetActive(true);
        }
    }
}
