using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    private Vector2 firstTouchPos;
    public bool inputEnabled = true;
    public PlayerController PlayerController;
  

    // Update is called once per frame
    void Update()
    {
        if (inputEnabled)
        {
            
            if(Input.touchCount > 0)
            {
                PlayerController.movementEnabled = true;
                Touch firstTouch = Input.GetTouch(0);
                
                if(firstTouch.phase == TouchPhase.Began)
                {
                    firstTouchPos = firstTouch.position;
                }

                var movement =  firstTouch.position.x -firstTouchPos.x ;
                if (movement>0.1f)
                {
                    if (transform.position.x<2.5f)
                    {
                        transform.position += new Vector3(movement* Time.deltaTime*0.1f,0,0);
                    }
                    //go right
                }
                else if (movement<=0.1f)
                {
                    if (transform.position.x>-2.5f)
                    {
                        transform.position += new Vector3(movement* Time.deltaTime*0.1f,0,0);
                    }
                    //go left
                }
                
                
            }  
        }
        
    }
}
